import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

import java.net.URISyntaxException;

public class test extends Application {
    public static void main(String[] args) throws URISyntaxException {
        //launch();

        Sound sound = new Sound();
    }
    @Override
    public void start(Stage stage) throws Exception {
        BorderPane pane = new BorderPane();
        stage.setScene(new Scene(pane, 480, 640));

        Image image = new Image("resources/background2.png");
        ImageView view = new ImageView(image);
        pane.getChildren().add(view);
        BirdWorld world = new BirdWorld();
        world.setPrefWidth(480);
        world.setPrefHeight(640);

        for(int i = 1; i <= 3; i++){

            Ground ground = new Ground();
            ground.setX(0 + i * ground.getWidth() / 2);
            ground.setY(640 - ground.getHeight());
            world.add(ground);
        }

        pane.setCenter(world);



        stage.show();
    }
}
