import javafx.scene.image.Image;

import java.net.URISyntaxException;

public class Ground extends Actor {
    double speed;
    public Ground(){
        String path = getClass().getClassLoader().getResource("resources/ground.png").toString();
        setImage(new Image(path));
        speed = 0.75;
    }
    @Override
    public void act(Long now) throws URISyntaxException {
        move(-speed, 0);

    }
}
