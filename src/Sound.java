import javafx.application.Application;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

import java.net.URISyntaxException;

public class Sound extends Application{
    public static void main(String[] args) {
        launch();
    }
    @Override
    public void start(Stage primaryStage) throws Exception {

    }

    public void playPoint() throws URISyntaxException {
        Media longMusic = new Media(getClass().getClassLoader().getResource("resources/point.mp3").toURI().toString());
        MediaPlayer musicPlayer =  new MediaPlayer(longMusic);
        musicPlayer.play();
    }


    public void playDeath() throws URISyntaxException {
        Media longMusic = new Media(getClass().getClassLoader().getResource("resources/die.mp3").toURI().toString());
        MediaPlayer musicPlayer =  new MediaPlayer(longMusic);
        musicPlayer.play();
    }

    public void playWing() throws URISyntaxException {
        Media longMusic = new Media(getClass().getClassLoader().getResource("resources/wing.mp3").toURI().toString());
        MediaPlayer musicPlayer =  new MediaPlayer(longMusic);
        musicPlayer.play();
    }

    public void playHit() throws URISyntaxException {
        Media longMusic = new Media(getClass().getClassLoader().getResource("resources/hit.mp3").toURI().toString());
        MediaPlayer musicPlayer =  new MediaPlayer(longMusic);
        musicPlayer.play();
    }

}
