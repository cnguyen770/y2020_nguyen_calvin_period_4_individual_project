import javafx.scene.image.Image;

public class BottomPipe extends Actor {
    double speedX;
    double speedY;
    double y;
    public BottomPipe(){
        String path = getClass().getClassLoader().getResource("resources/Pipe2.png").toString();
        setImage(new Image(path));
        speedX = 0.85;
        speedY = 0;
        y = this.getY();
    }
    @Override
    public void act(Long now) {
        //condition to make the pipe move up and down
        if(this.getY() > y + 5){
            move(-speedX, -speedY);
        }
        else{
            move(-speedX, speedY);
        }
    }

    public void setSpeedX(int x){
        speedX = x;
    }

    public void setSpeedY(int y){
        speedY = y;
    }

    public double getSpeedX(){
        return this.speedX;
    }

    public double getSpeedY(){
        return this.speedY;
    }

}
