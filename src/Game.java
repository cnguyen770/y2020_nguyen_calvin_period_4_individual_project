import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import  javafx.scene.image.*;


public class Game extends Application{
    Bird bird;
    BirdWorld world;

    public static void main(String[] args) {
        launch();
    }
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Flappy Bird");


        BorderPane pane = new BorderPane();
        stage.setScene(new Scene(pane, 480, 640));

        Image image = new Image("resources/background2.png");
        ImageView view = new ImageView(image);
        pane.getChildren().add(view);

        //--------------------------initialize objects-----------------------------
        bird = new Bird();
        bird.setX(240 - bird.getWidth());
        bird.setY(320 + bird.getHeight());

        Coin coin = new Coin();
        coin.setX(240);
        coin.setY(100);

        BottomPipe pipe = new BottomPipe();
        pipe.setX(400);
        pipe.setY(200);

        world = new BirdWorld();
        world.setPrefWidth(480);
        world.setPrefHeight(640);

        Ground ground = new Ground();
        ground.setX(0);
        ground.setY(640 - ground.getHeight());

        world.add(pipe);
        world.add(ground);
        world.add(bird);
        world.add(coin);
        world.start();

        pane.setCenter(world);
        stage.show();
        world.requestFocus();

        //runGame();





        world.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                world.addKey(event.getCode());
            }
        });

        world.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                world.removeKey(event.getCode());
            }
        });
    }

    public void runGame(){
        //loop to run the game
        do{
            int i = 0;
            Ground ground = new Ground();
            ground.setX(0 + i * ground.getWidth() / 2);
            ground.setY(640 - ground.getHeight());
            world.add(ground);
            i++;
            if(bird.getIntersectingObjects(ground.getClass()).size() != 0){
                bird.gameOver = true;
            }
            if(i == 5){
                bird.gameOver = true;
            }
        }while(!bird.isGameOver());
    }


}
