import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Score extends Text {

    private int scoreValue;

    public Score() {
        //initialize score to 0
        scoreValue = 0;
        //sets font size to something larger than the default
        this.setFont(Font.font("Verdana", 50));
        updateDisplay();
    }

    public void updateDisplay() {
        //sets text of this Node to the current value of the score attribute;
        this.setText("" + scoreValue);
    }

    public int getScoreValue() {
        return scoreValue;
    }


    public void setScoreValue(int scoreValue) {
        this.scoreValue = scoreValue;
        updateDisplay();
    }

}
