import javafx.scene.image.Image;

import java.net.URISyntaxException;

public class TopPipe extends Actor{
    double speedX;
    double speedY;
    double y;
    public TopPipe(){
        String path = getClass().getClassLoader().getResource("resources/Pipe3.png").toString();
        setImage(new Image(path));
        speedX = 0.85;
        speedY = 0;
        y = this.getY();
    }
    @Override
    public void act(Long now) throws URISyntaxException {
        //condition to make the pipe move up and down
        if(this.getY() > y + 5){
            move(-speedX, -speedY);
        }
        else{
            move(-speedX, speedY);
        }
    }

    //returns the y pos of the bottom left of the pipe
    public double getYPos(){
        return this.getY() + this.getHeight();
    }

    public void setSpeedX(int x){
        speedX = x;
    }

    public void setSpeedY(int y){
        speedY = y;
    }
}
