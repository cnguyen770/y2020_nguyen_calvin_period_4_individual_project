import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.animation.AnimationTimer;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;

public abstract class World extends Pane {
    private AnimationTimer timer;
    private HashSet key;

    public World(){

        timer = new AnimationTimer() {
            @Override
            public void handle(long now){
                act(now);
                ObservableList<Node> list = getChildren();
                ArrayList<Actor> actor = new ArrayList<>();
                for (Node node : list) {
                    if(node instanceof Actor){
                        actor.add((Actor)node);
                    }
                }
                for(Actor e : actor){
                    try {
                        e.act(now);
                    } catch (URISyntaxException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        };
        key = new HashSet();
        start();
    }

    public void addKey(KeyCode e){
        key.add(e);
    }

    public void removeKey(KeyCode e){
        key.remove(e);
    }

    public boolean isKeyDown(KeyCode e){
        if(key.contains(e)){
            return true;
        }
        return false;
    }

    public abstract void act(Long now);

    public void add(Actor actor){
        this.getChildren().add(actor);
    }

    public void remove(Actor actor){
        this.getChildren().remove(actor);
    }

    public void start(){
        timer.start();

    }
    public void stop(){
        timer.stop();
    }
    public <A extends Actor> java.util.List<A> getObjects(java.lang.Class<A> cls){
        ArrayList<A> list = new ArrayList<>();
        ObservableList<Node> nodes = this.getChildren();
        for(Node node : nodes){
            if(node.getClass().isInstance(cls)){
                list.add((A)node);
            }
        }
        return list;
    }


}
class BirdWorld extends World{
    Score score;

    public BirdWorld(){
        score = new Score();
        score.setX(222);
        score.setY(150);
        getChildren().add(score);
    }

    public Score getScore(){
        return score;
    }

    @Override
    public void act(Long now) {

    }
}
