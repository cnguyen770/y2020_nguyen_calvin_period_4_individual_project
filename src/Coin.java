import javafx.scene.image.Image;

public class Coin extends Actor {

    public Coin(){
        String path = getClass().getClassLoader().getResource("resources/Coin.png").toString();
        setImage(new Image(path));
    }

    @Override
    public void act(Long now) {

    }
}
