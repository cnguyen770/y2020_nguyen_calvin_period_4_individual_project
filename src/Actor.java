import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public abstract class Actor extends ImageView{

    public abstract void act(Long now) throws URISyntaxException;
    public void move(double dx, double dy){
        this.setX(getX() + dx);
        this.setY(getY() + dy);
    }


    public World getWorld(){
        return (World)this.getParent();
    }

    public double getHeight(){
        Bounds bounds = this.getBoundsInParent();
        return bounds.getHeight();
    }

    public double getWidth(){
        Bounds bounds = this.getBoundsInParent();
        return bounds.getWidth();
    }

    public <A extends Actor> java.util.List<A> getIntersectingObjects(java.lang.Class<A> cls){
        ArrayList<A> list = new ArrayList<>();
        ArrayList<Actor> e = new ArrayList<Actor>();
        ObservableList<Node> nodes = this.getParent().getChildrenUnmodifiable();
        for(Node i : nodes){
            if(i.getBoundsInParent().intersects(this.getBoundsInParent()) && i != this && cls.isInstance(i)){
                list.add((A)i);
            }
        }
        return list;
    }

    public <A extends Actor> A getPipeCrossed(java.lang.Class<A> cls){
        List<A> list = new ArrayList<>();
        ObservableList<Node> nodes = this.getParent().getChildrenUnmodifiable();
        for(Node e : nodes){

        }
        return null;
    }

    public <A extends Actor> A getOneIntersectingObject(java.lang.Class<A> cls){
        List<A> list = getIntersectingObjects(cls);
        for(A i : list){
            return i;
        }
        return null;
    }





}
class Bird extends Actor{
    double dx;
    double dy;
    double speed;
    boolean flag = false;
    boolean bool = false;
    boolean gameOver = false;
    public Bird(){
        String path = getClass().getClassLoader().getResource("resources/Flappybird.png").toString();
        setImage(new Image(path));
        dx = 1;
        dy = 1;
        speed = 2;

    }

    public boolean isGameOver(){
        return gameOver;
    }


    @Override
    public void act(Long now) throws URISyntaxException {
        Sound sound = new Sound();

        //plays sound and removes coin from the world
        if(this.getOneIntersectingObject(Coin.class) != null){
            Coin coin = getOneIntersectingObject(Coin.class);
            BirdWorld b = (BirdWorld)getWorld();
            b.getScore().setScoreValue(b.getScore().getScoreValue() + 1);
            getWorld().remove(coin);
            sound.playPoint();
        }





        if(this.getIntersectingObjects(Ground.class).size() != 0){
            sound.playDeath();
            this.setY(565 - this.getHeight());
            gameOver = true;
            getWorld().stop();
        }

        if(this.getIntersectingObjects(BottomPipe.class).size() != 0){
            sound.playDeath();
            gameOver = true;
            getWorld().stop();
        }

        if(!getWorld().isKeyDown(KeyCode.SPACE)){
            move(0, 2);
        }
        if(this.getX() + this.getWidth() >= this.getWorld().getWidth()){
            this.setX(this.getWorld().getWidth() - this.getWidth());
        }
        if(getWorld().isKeyDown(KeyCode.SPACE)){
            //sound.playWing();
            move(0,-5);
        }


    }
}

