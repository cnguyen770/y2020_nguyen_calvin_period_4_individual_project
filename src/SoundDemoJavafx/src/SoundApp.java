package SoundDemoJavafx.src;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;

import javax.sound.midi.Sequencer;
import javax.swing.JButton;
import javax.swing.JFrame;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Duration;

public class SoundApp extends Application {

	// Used for MIDI files
	File midiFile;
	Sequencer sequencer;

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Sound Demo JavaFX");
		// Load WAV file
		// AudioClip works well for short sounds that need to be played a lot.
		// It will not work for long sounds, such as a music mp3
		//AudioClip coinSound = Applet.newAudioClip(getClass().getClassLoader().getResource("resources/smw_coin.wav"));
		
		// MediaPlayer is good for longer sounds like background music mp3s
		Media longMusic = new Media(getClass().getClassLoader().getResource("resources/hit.mp3").toURI().toString());
		MediaPlayer musicPlayer =  new MediaPlayer(longMusic);
		musicPlayer.setCycleCount(1); // plays once
		//musicPlayer.setCycleCount(Integer.MAX_VALUE); // loops forever
		
		HBox root = new HBox();
		Button coinBtn = new Button("Coin");
		coinBtn.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				//coinSound.play();
			}
		});
		root.getChildren().add(coinBtn);
		
		Button startMusicBtn = new Button("Play Music");
		startMusicBtn.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				// start over at the beginning (needed if media finished playing)
				musicPlayer.seek(Duration.ZERO);
				musicPlayer.play();
			}
		});
		root.getChildren().add(startMusicBtn);
		
		ToggleButton loopMusicBtn = new ToggleButton("Turn Repeat On");
		loopMusicBtn.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				// toggle between repeat and no repeat
				if (musicPlayer.getCycleCount() == Integer.MAX_VALUE) {
					musicPlayer.setCycleCount(1);
					loopMusicBtn.setText("Turn Repeat On");
				} else {
					musicPlayer.setCycleCount(Integer.MAX_VALUE);
					loopMusicBtn.setText("Turn Repeat Off");
				}
			}
		});
		root.getChildren().add(loopMusicBtn);
		
		Button stopMusicBtn = new Button("Stop Music");
		stopMusicBtn.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				// Stop play (not the same as pausing)
				musicPlayer.stop();
			}
		});
		root.getChildren().add(stopMusicBtn);
		
		Button forwardMusicBtn = new Button("Forward 5 min");
		forwardMusicBtn.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				// skip forward 5 minutes (useful for testing loop)
				musicPlayer.seek(Duration.minutes(5));
			}
		});
		root.getChildren().add(forwardMusicBtn);
		
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show();
	}

}
