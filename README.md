# README #
I am still working on the game but I need some more time. I have finished a 
decent amount already such as all the images and resizing as well as the
implementation of sound and the objects properties.

**Name:**	Nguyen Calvin

**Period:**	4

**Game Title:** Flappy Bird

## Game Proposal ##

This is a game that already exists called Flappy Bird and it includes a bird that the player has to get through all of the obstacles(in this game they are pipes)
and the user has to time the birds jumps or flaps to get passed the obstacles that are at random heights. If the bird touches the ground or an obstacle the game is lost.
The game is a bit simple but I may add things such as saving your score (as a text file) or an ability to add the score to a leaderboard (also as a text file) and
be able to access the leaderboard from the menu.

Game Controls:

space bar is to jump

Game Elements:

The bird
obstacles

How to Win:

There is no winning in this game. The objective is to pass as many obstacles as possible and get a new high score

## Link Examples ##
Provide links to examples of your game idea.  This can be a playable online game, screenshots, YouTube videos of gameplay, etc.

https://flappybird.io/

## Teacher Response ##

**Approved**

This is a classic game and has good potential as long as you don't make a super simple version with levels that just get faster
with more pipes. There has to be some kind of progression that is interesting rather than just speeding up the pipes. Also, an important
factor in your game is whether Flappy Bird has actually touched a pipe. Normally, if the rectangular bounding box of Flappy Bird touches
the bounding box of a pipe, it counts as touching in JavaFX. However, players won't like this at all because Flappy Bird will die when it
looks like it's NOT touching. The simplest solution to this is having a second, smaller sprite always follow Flappy Bird.  This sprite
can be completely transparent so the player doesn't see it but it would basically be a hitbox within Flappy Bird that counts as "touching."
You would use that sprite to detect pipe collisions and it will give the player some leeway for what it means for Flappy Bird to touch
a pipe.  If you want a challenge, you could also try a pixel-perfect collision detection technique which I've outlined at the end of the
project slides.  But for additional features you could always have things like spinning coins to collect, etc.


## Class Design and Brainstorm ##

Put all your brainstorm ideas, strategy approaches, and class outlines here

I am planning to add pixel perfect collision as for this game it seems like
that will be the most appropriate as the hit boxes in this game are probably
the most complicated as well as important for the player.
I also plan to add all necessary sound effects
I plan to make it so that the bird does not continuously to the right but
the pipes will move continuously to the left at random heights.
To make the game more complex I will try to increase the speed as more points
are added and even pipes that will move up and down. I will also be adding coins. 
## Development Journal ##

Every day you work, keep track of it here.

**Date (time spent)**

5/26/2020
2 hours

Goal:  What are you trying to accomplish today?

setup the basics of the game

import all images and sound effects and set up some of their propeties

Work accomplished:  Describe what you did today and how it went.

I found all of the images that I wanted and I also added the sound demo
and figured out which sound effects I will use as well as how to use them
I set up a background as well as a point system that is not yet complete
but will add a point when a coin is touched. I have also set up the sizes for
most of the images using the GIMP tutorial posted.

**Date (time spent)**

Goal:  What are you trying to accomplish today?

Work accomplished:  Describe what you did today and how it went.

***
***


